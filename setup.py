# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open("README.md") as f:
    README = f.read()

with open("LICENSE") as f:
    LICENSE = f.read()

setup(
    name="i3man",
    version="0.1.5",
    description="A few helpers to improve my experience with i3.",
    long_description=README,
    author="Ryad ZENINE",
    author_email="r.zenine@gmail.com",
    url="https://gitlab.com/r.zenine/i3man",
    install_requires=["i3-py", "click", "pulsectl"],
    entry_points={"console_scripts": ["i3man = i3man.cli:cli"]},
    license=LICENSE,
    packages=find_packages(exclude=("tests", "docs")),
)
