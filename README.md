# I3man module repository

I3man is a simple tool that adds some goodies to the i3 window manager.

## Features : 

### Media Keys: 

* Keyboard Back-light management.

```
i3man keyboard-backlight inc # Increases keyboard backlight
i3man keyboard-backlight dec # Decreases keyboard backlight
```

* Volume control.

```
i3man audio inc # Increase volume on the current sink
i3man audio dec # Decreases volume on the current sink
```

* Screen Back-light management ( Coming soon ).

### Multiple Screens: 

* Move workspace to the next screen.
```
i3man workspace move
```

* Configure multiple screen views (Coming soon).

### Window Layouts:

* Save and restore layouts ( of the current workspace or all workspace ) (Coming Soon).
