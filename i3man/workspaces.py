"""Packages that provides usefull methods for managing i3 workspaces"""
import i3


def move_workspace_next_output():
    """this command moves the current active workspaces
    to the next available output"""
    active_outputs = list(filter(lambda x: x[u"active"], i3.get_outputs()))

    if len(active_outputs) == 1:
        return

    current_workspace_index = 0

    workspaces = i3.get_workspaces()

    for i, wkp in enumerate(workspaces):
        if wkp["focused"]:
            current_workspace_index = i

    current_workspace = workspaces[current_workspace_index]

    cw_output_name = current_workspace["output"]

    cw_output_index = 0
    for i, out in enumerate(active_outputs):
        if out["name"] == cw_output_name:
            cw_output_index = i
    if len(active_outputs) - 1 == cw_output_index:
        i3.move("workspace to output", active_outputs[0]["name"])
    else:
        i3.move("workspace to output", active_outputs[cw_output_index + 1]["name"])

    i3.command("workspace " + current_workspace["name"])
