#! /usr/bin/python
import os
import argparse
import click


def get_keyboard_backlight_root_directory(default_location="/sys/class/leds"):
    candidates = [i for i in os.listdir(default_location) if "kbd_backlight" in i]
    if len(candidates) != 1:
        return None
    else:
        return os.path.join(default_location, candidates[0])


def read_max_brightness(kbd_backlight_directory):
    with open(os.path.join(kbd_backlight_directory, "max_brightness")) as f:
        v = int(f.readlines()[0][0])
    return v


def read_current_brightness(kbd_backlight_directory):
    with open(os.path.join(kbd_backlight_directory, "brightness")) as f:
        v = int(f.readlines()[0][0])
    return v


def update_brightness(kbd_backlight_directory, value):
    with open(os.path.join(kbd_backlight_directory, "brightness"), "w") as f:
        f.write(str(value))


def increase_brightness(kbd_backlight_directory):
    max_b = read_max_brightness(kbd_backlight_directory)
    current_b = read_current_brightness(kbd_backlight_directory)

    new_value = min(current_b + 1, max_b)

    if new_value != current_b:
        update_brightness(kbd_backlight_directory, new_value)


def decrease_brightness(kbd_backlight_directory):
    max_b = read_max_brightness(kbd_backlight_directory)
    current_b = read_current_brightness(kbd_backlight_directory)

    new_value = max(current_b - 1, 0)

    if new_value != current_b:
        update_brightness(kbd_backlight_directory, new_value)


def define_parser():
    parser = argparse.ArgumentParser(description="Handle keyboard backlight.")
    parser.add_argument(
        "action",
        choices=["inc", "dec"],
        help="inc to increase (dec to decrease) keyboard brightness",
    )

    return parser


def inc():
    d = get_keyboard_backlight_root_directory()
    increase_brightness(d)


def dec():
    d = get_keyboard_backlight_root_directory()
    decrease_brightness(d)
