"""Command line package for i3man."""
import click
import i3man


@click.group()
def cli():
    """CLI entry point"""


@cli.group(help="Pulse audio subcommands.")
def audio():
    """audio subcommands entry point"""


@audio.command(help="Prints to stdout the current audio status.")
def status():
    """Display the audio status. (to be used by i3 blocks for ex)"""
    print(i3man.audio.status())


@audio.command("inc", help="Increments volume on default sink by 10%.")
def inc_volume():
    """increases the volume of the current audio sink"""
    i3man.audio.inc_volume()


@audio.command("dec", help="Decrements volume on default sink by 10%.")
def dec_volume():
    """decreases the volume of the current audio sink"""
    i3man.audio.dec_volume()


@audio.command("mute", help="Toggle mute on the default sink by 10%.")
def mute_volume():
    """mute the volume of the current audio sink"""
    i3man.audio.mute()


@cli.group("keyboard-backlight", help="Keyboad backlight commands.")
def keyboard_backlight():
    """keyboard backlight sub-commands"""


@keyboard_backlight.command("inc", help="Increase the keyboard backlight brightness.")
def inc_backlight():
    """increases the keyboard backlight."""
    i3man.kbd_backlight.inc()


@keyboard_backlight.command("dec", help="Decrease the keyboard backlight brightness.")
def dec_backlight():
    """decreases the keyboard backlight."""
    i3man.kbd_backlight.dec()


@cli.group(help="I3 workspaces related commands.")
def workspaces():
    """i3 workspace management sub-commands"""


@workspaces.command(
    "move", help="move the focused workspace to the next available Output."
)
def move_workspace():
    """move the current active workspace to the next output."""
    i3man.workspaces.move_workspace_next_output()


if __name__ == "__main__":
    cli()
