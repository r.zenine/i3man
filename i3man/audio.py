"""Audio management functions"""
import pulsectl

STATUS_FORMAT = u"{sink_description} : {volume} %"
STATUS_FORMAT_MUTE = u"{sink_description} : Mute"


def get_default_sink(pulse: pulsectl.PulseSinkInfo):
    """Finds the current active pulse audio sink"""
    sinks = set(pulse.sink_list())
    default_name = pulse.server_info().default_sink_name
    for s in sinks: 
        if s.name == default_name:
            return s


def __format_output(pulse: pulsectl.PulseSinkInfo):
    """Formats the output to work well with i3 blocks"""
    default_sink = get_default_sink(pulse)
    if len(default_sink.description.split(" ")) > 2:
        desc = " ".join(default_sink.description.split(" ")[:2])
    else:
        desc = default_sink.description
    data = {
        "sink_description": desc,
        "volume": int(default_sink.volume.value_flat * 100),
    }
    if default_sink.mute:
        return STATUS_FORMAT_MUTE.format(**data)
    return STATUS_FORMAT.format(**data)


def status():
    """outputs the current status of audio. The output is intended to be used
        in i3-blocks
    """
    pulse: pulsectl.Pulse = pulsectl.Pulse("i3man")
    fmt: str = __format_output(pulse)
    return fmt


def inc_volume() -> None:
    """Increases the volume of the default pulse audio sink"""
    pulse: pulsectl.Pulse = pulsectl.Pulse("i3man")
    default_sink: pulsectl.PulseSinkInfo = get_default_sink(pulse)
    vol: pulsectl.PulseVolumeInfo = default_sink.volume
    vol.value_flat: float = min(vol.value_flat * 1.10, 1)
    pulse.volume_set(default_sink, vol)


def dec_volume():
    """Decreases the volume of the default pulse audio sink"""
    pulse: pulsectl.Pulse = pulsectl.Pulse("i3man")
    default_sink: pulsectl.PulseSinkInfo = get_default_sink(pulse)
    vol: pulsectl.PulseVolumeInfo = default_sink.volume
    vol.value_flat: float = max(vol.value_flat * 0.90, 0)
    pulse.volume_set(default_sink, vol)


def mute() -> None:
    """Mutes the volume of the default pulse audio sink"""
    pulse: pulsectl.Pulse = pulsectl.Pulse("i3man")
    default_sink: pulsectl.PulseSinkInfo = get_default_sink(pulse)
    pulse.mute(default_sink, not default_sink.mute)
