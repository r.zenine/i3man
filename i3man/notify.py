import os

_AVAILABLE_URGENCY = ["low", "normal", "critical"]


def notify_send(title, message, urgency="low", category=""):
    urg = urgency if urgency in _AVAILABLE_URGENCY else _AVAILABLE_URGENCY[0]
    data = {"title": title, "urgency": urg, "category": category, "message": message}
    os.system(
        'notify-send -u {urgency} -c {category} "{title}" "{message}"'.format(**data)
    )
